# -*- coding: utf-8 -*-
from sqlalchemy import *
from sqlalchemy.orm import *
import ConfigParser

Config = ConfigParser.ConfigParser()
Config.read(["../db.ini", "db.ini", "/home/cfz/cfz-app/cfz-persistence/db.ini"])
sqlalchemy_url = Config.get('db','url')

#sqlalchemy_url = 'postgres://cfzadmin:cfz4dm1n@localhost:5432/cfz'
engine = create_engine(sqlalchemy_url)
maker = sessionmaker(autoflush=True, autocommit=False)
DBSession = scoped_session(maker)


def init_model():
    DBSession.configure(bind=engine)
