from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy import Sequence, ForeignKey, Index
from sqlalchemy.schema import Table
from sqlalchemy.types import DateTime, UnicodeText, Unicode, Float, Numeric
from sqlalchemy.orm import mapper, relationship, configure_mappers
from sqlalchemy.ext.associationproxy import association_proxy

from datetime import datetime

from cfzpersistence.config import init_session
from cfzcore.person import Person
from cfzcore.application import Application, ApplicationInvestor
from cfzcore.company import (Company,
                             CompanyAnnualReport,
                             Director,
                             RepresentationType,
                             CompanyType,
                             AuthorizedRepresentative)
from cfzcore.shareholder import Shareholder
from cfzcore.vehicle_pass import VehiclePass
from cfzcore.user_account import UserGroup
from cfzcore.user_account import UserAccount
from cfzcore.goods import GoodsCategory
from cfzcore.outlet import (Outlet, OutletVehiclepass)
from cfzcore.address import Address
from cfzcore.company.passes import CourtesyPass
from cfzcore.company.docs.contracts import OperationalContract
from cfzcore.letters import (
    AccessLetter,
    AccessLetterType)


init_session()


def do_map(metadata):

    person_table = Table('person', metadata,
                         Column('id', Integer, Sequence('person_id_seq'), primary_key=True),
                         Column('first_name', Unicode(50), nullable=False),
                         Column('last_name', Unicode(50), nullable=False),
                         Column('email', Unicode),
                         Column('phone1', Unicode),
                         Column('phone2', Unicode),
                         keep_existing=True)

    user_account_table = Table('user_account', metadata,
                               Column('id', Integer, Sequence('user_account_id_seq'), primary_key=True),
                               Column('username', UnicodeText, nullable=False),
                               Column('email', UnicodeText, nullable=False),
                               Column('passphrase', UnicodeText, nullable=False),
                               Column('active', Boolean, default=True),
                               extend_existing=True)

    user_group_table = Table('user_group', metadata,
                             Column('id', Integer, Sequence('user_group_id_seq'), primary_key=True),
                             Column('group_name', UnicodeText, nullable=False),
                             extend_existing=True)

    user_group_association_table = Table('user_group_association', metadata,
                                         Column('user_account_id', Integer, ForeignKey('user_account.id'), primary_key = True),
                                         Column('user_group_id', Integer, ForeignKey('user_group.id'), primary_key = True),
                                         extend_existing=True)

    address_table = Table('address', metadata,
                           Column('id', Integer, Sequence('address_id_seq'), primary_key=True),
                           Column('line1', UnicodeText, nullable=False),
                           Column('line2', UnicodeText),
                           Column('latitude', Float),
                           Column('longitude', Float),
                           keep_existing=True)

    company_table = Table('company', metadata,
                          Column('id', Integer, Sequence('company_id_seq'), primary_key=True),
                          Column('name', Unicode(30), nullable=False, index=True),
                          Column('registration_number', Unicode(30), nullable=False, index=True),
                          #Column('company_type', Unicode(30), nullable=True),
                          Column('cell_number', Unicode(15), nullable=True),
                          Column('fax_number', Unicode(15), nullable=True),
                          Column('telephone_number', Unicode, nullable=True),
                          Column('description_of_goods', UnicodeText, nullable=True),
                          Column('state', Unicode(20), nullable=True),
                          Column('country', Unicode(30), nullable=True),
                          Column('status', Unicode(20), nullable=True),
                          Column('date_sent_letter_of_acceptance', DateTime),
                          Column('max_vehicles', Integer, default=6),
                          Column('incorporation_date', DateTime, nullable=True),
                          Column('address_id', Integer, ForeignKey(address_table.c.id)),
                          Column('active', Boolean, default=False),
                          Column('deleted', Boolean, default=False, index=True),
                          Column('created_date', DateTime, default=datetime.now()),
                          keep_existing=True)

    outlet_table = Table('outlet', metadata,
                         Column('id', Integer, Sequence('outlet_id_seq'), primary_key=True),
                         Column('name', UnicodeText, nullable=False, index=True),
                         Column('status', UnicodeText, nullable=False),
                         Column('telephone_number', UnicodeText),
                         Column('fax_number', UnicodeText),
                         Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                         Column('address_id', Integer, ForeignKey(address_table.c.id)),
                         Column('manager_id', Integer, ForeignKey(person_table.c.id)),
                         Column('start_date', DateTime),
                         Column('created_date', DateTime, default=datetime.now()),
                         Column('active', Boolean, default=True),
                         keep_existing=True)

    director_table = Table('director', metadata,
                           Column('id', Integer, Sequence('shareholder_id_seq'), primary_key=True),
                           Column('person_id', Integer, ForeignKey(person_table.c.id), nullable=False),
                           Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                           Column('start_date', DateTime, default=datetime.now()),
                           Column('end_date', DateTime, default=None),
                           Column('active', Boolean, default=False),
                           keep_existing=True)

    manager_table = Table('manager', metadata,
                           Column('id', Integer, Sequence('shareholder_id_seq'), primary_key=True),
                           Column('person_id', Integer, ForeignKey(person_table.c.id), nullable=False),
                           Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                           Column('start_date', DateTime, default=datetime.now()),
                           Column('end_date', DateTime, default=None),
                           keep_existing=True)

    application_table = Table('application', metadata,
                              Column('id', Integer, Sequence('application_seq_id'), primary_key=True),
                              Column('status', String(15), nullable=False),
                              Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                              Column('state', String(50), nullable=True),
                              Column('country', String(70)),
                              Column('description_of_goods', String(200), nullable=True),
                              Column('submission_date', DateTime()),
                              Column('consultant_id', Integer, ForeignKey(person_table.c.id)),
                              Column('reason_for_denial', String, nullable=True),
                              Column('created_date', DateTime, default=datetime.now()),
                              Column('approved_date', DateTime, nullable=True),
                              Column('denied_date', DateTime, nullable=True),
                              Index('idx_application_status', 'status'),
                              keep_existing=True)

    application_investor_table = Table('application_investor', metadata,
                                       Column('id', Integer, Sequence('application_investor_seq_id'), primary_key=True),
                                       Column('application_id', None, ForeignKey(application_table.c.id)),
                                       Column('person_id', None, ForeignKey(person_table.c.id)),
                                       Column('percent_owned', Integer, nullable=False),
                                       Column('background_check', String),
                                       keep_existing=True)

    shareholder_table = Table('shareholder', metadata,
                              Column('id', Integer, Sequence('shareholder_seq_id'), primary_key=True),
                              Column('company_id', None, ForeignKey(company_table.c.id)),
                              Column('person_id', None, ForeignKey(person_table.c.id)),
                              Column('shares', Integer, nullable=False),
                              Column('background_check', String),
                              Column('created_date', DateTime, default=datetime.now()),
                              Column('modified_date', DateTime, default=datetime.now()),
                              Column('active', Boolean, default=True),
                              keep_existing=True)

    representation_type_table = Table('representation_type', metadata,
                                      Column('id', Integer, Sequence('representation_type_seq_id'), primary_key=True),
                                      Column('name', Unicode, nullable=False),
                                      keep_existing=True)

    authorized_rep_table = Table('authorized_representative', metadata,
                                 Column('id', Integer, Sequence('authorized_representative_seq_id'), primary_key=True),
                                 Column('person_id', None, ForeignKey(person_table.c.id)),
                                 Column('company_id', None, ForeignKey(company_table.c.id)),
                                 Column('representation_type_id', None, ForeignKey(representation_type_table.c.id)),
                                 Column('signature', Unicode, nullable=True),
                                 Column('date_created', DateTime, default=datetime.now()),
                                 Column('date_deleted', DateTime, default=None),
                                 Column('active', Boolean, default=True),
                                 keep_existing=True)

    company_annual_report_table = Table("company_annual_report", metadata,
                                        Column("id", Integer, Sequence("company_annual_report_seq_id"), primary_key=True),
                                        Column("company_id", None, ForeignKey(company_table.c.id)),
                                        Column("date_submitted", DateTime, nullable=False),
                                        Column("reporting_year", Integer, nullable=False),
                                        keep_existing=True)

    vehicle_pass_table = Table("vehicle_pass", metadata,
                               Column('id', Integer, Sequence('company_id_seq'), primary_key=True),
                               Column('owner', UnicodeText, nullable=False),
                               Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                               Column('vehicle_year', Integer, nullable=False),
                               Column('vehicle_model', Unicode, nullable=False),
                               Column('color', Unicode, nullable=False),
                               Column('license_plate', Unicode, nullable=False),
                               Column('date_issued', DateTime, nullable=False),
                               Column('sticker_number', Unicode, nullable=False),
                               Column('receipt_number', Unicode, nullable=False),
                               Column('issued_by', Unicode),
                               Column('month_expired', Unicode, nullable=False),
                               Column('expired', Boolean, default=False),
                               Column('active', Boolean, default=True),
                               keep_existing=True
                               )

    courtesy_pass_table = Table("courtesy_pass", metadata,
                               Column('id', Integer, Sequence('company_id_seq'), primary_key=True),
                               Column('_person', UnicodeText, nullable=False),
                               Column('company_id', Integer, ForeignKey(company_table.c.id), nullable=False),
                               Column('vehicle_year', Integer, nullable=False),
                               Column('vehicle_model', Unicode, nullable=False),
                               Column('color', Unicode, nullable=False),
                               Column('license_plate', Unicode, nullable=False),
                               Column('date_issued', DateTime, nullable=False),
                               Column('sticker_number', Unicode, nullable=False),
                               Column('month_expired', Unicode, nullable=False),
                               Column('expired', Boolean, default=False),
                               Column('_authorized_by', Unicode, nullable=False),
                               Column('_issued_by', Unicode, nullable=False),
                               Column('active', Boolean, default=True),
                               keep_existing=True
                               )

    goods_categories_table = Table("goods_categories", metadata,
                                   Column("id", Integer, Sequence('goods_categories_id_seq'), primary_key=True),
                                   Column("name", Unicode, unique=True, nullable=False),
                                   Column("date_created", DateTime, default=datetime.now()),
                                   Column("date_modified", DateTime, default=datetime.now()),
                                   Column("active", Boolean, default=True),
                                   keep_existing=True
                                   )

    company_goods_categories_table = Table("company_goods_categories", metadata,
                                           Column('company_id', Integer, ForeignKey('company.id'), primary_key=True),
                                           Column('goods_category_id', Integer, ForeignKey('goods_categories.id'), primary_key=True),
                                           extend_existing=True)

    company_type_table = Table("company_types", metadata,
                               Column('id', Integer, Sequence('company_types_id_seq'), primary_key=True),
                               Column('_name', Unicode(30), nullable=False),
                               Column('date_created', DateTime, default=datetime.now()),
                               Column('date_modified', DateTime, default=datetime.now()),
                               Column('active', Boolean, default=True),
                               keep_existing=True
                         )

    company_types_map_table = Table("company_types_map", metadata,
                                    Column('company_id', Integer, ForeignKey('company.id'), primary_key=True),
                                    Column('company_type_id', Integer, ForeignKey('company_types.id'), primary_key=True),
                                    Column('date_created', DateTime, default=datetime.now()),
                                    Column('date_modified', DateTime, default=datetime.now()),
                                    Column('active', Boolean, default=True),
                                    keep_existing=True
                            )

    operational_contracts_table = Table("operational_contracts", metadata,
                                       Column('id', Integer, Sequence('operational_contract_id_seq'), primary_key=True),
                                       Column('company_id', Integer, ForeignKey('company.id'), primary_key=True),
                                       Column('cfz_ceo', Unicode(50), nullable=False),
                                       Column('director', Unicode(50), nullable=False),
                                       Column('cfz_office', UnicodeText, nullable=False),
                                       Column('company_types', UnicodeText, nullable=False),
                                       Column('goods', UnicodeText, nullable=False),
                                       Column('seal_date', DateTime, nullable=False),
                                       Column('processing_fee', Numeric(8, 2), nullable=False),
                                       Column('date_created', DateTime, default=datetime.now()),
                                       Column('date_modified', DateTime, default=datetime.now()),
                                       Column('active', Boolean, default=True),
                                       keep_existing=True
                        )

    access_letter_table = Table("access_letters", metadata,
        Column('id', Integer, Sequence('acceptance_letter_id_seq'), primary_key=True),
        Column('place_of_origin', Unicode(50), nullable=True),
        Column('granted_to', Unicode(100), nullable=False),
        Column('vehicle_license_plate', Unicode(100), nullable=False),
        Column('processing_fee', Unicode(6), nullable=False),
        Column('receipt_id', Unicode(25), nullable=False),
        Column('authorized_by', Unicode(50), nullable=False),
        Column('issued_by', Unicode(50), nullable=False),
        Column('expiration_date', DateTime, default=datetime.now()),
        Column('date_processed', DateTime, default=datetime.now()),
        Column('letter_type', Integer, default=AccessLetterType.courtesy),
        Column('active', Boolean, default=True),
        extend_existing=True
    )


    mapper(Person, person_table)
    mapper(Address, address_table)
    mapper(AccessLetter, access_letter_table)
    mapper(Shareholder, shareholder_table, properties={
        'person': relationship(Person),
        'company': relationship(Company)})
    mapper(Director, manager_table, properties={
        'person': relationship(Person),
        'company': relationship(Company)
        })
    mapper(Outlet, outlet_table, properties={
        'company': relationship(Company),
        'manager': relationship(Person),
        'address': relationship(Address)
    })

    mapper(CompanyAnnualReport, company_annual_report_table, properties={
        'company': relationship(Company)})
    mapper(GoodsCategory, goods_categories_table)
    mapper(CompanyType, company_type_table)
    mapper(OperationalContract, operational_contracts_table, properties={
        'company': relationship(Company)
    })
    mapper(Company, company_table, properties={
        'directors': relationship(Director),
        'shareholders': relationship(Shareholder),
        'annual_reports': relationship(CompanyAnnualReport),
        'address': relationship(Address),
        'courtesy_passes': relationship(CourtesyPass),
        'operational_contracts': relationship(OperationalContract),
        'goods_categories': relationship(GoodsCategory,
                                         secondary=lambda: company_goods_categories_table),
        'company_types': relationship(CompanyType,
                                         secondary=lambda: company_types_map_table),
    })
    mapper(RepresentationType, representation_type_table)
    mapper(AuthorizedRepresentative, authorized_rep_table, properties={
        'company': relationship(Company),
        'person': relationship(Person),
        'representation_type': relationship(RepresentationType)
    })
    mapper(ApplicationInvestor, application_investor_table, properties={
        'application': relationship(Application),
        'person': relationship(Person, cascade="all")})
    mapper(Application, application_table, properties={
        'company': relationship(Company, order_by=[company_table.c.name]),
        'consultant': relationship(Person),
        'investors': relationship(ApplicationInvestor)})
    mapper(VehiclePass, vehicle_pass_table, properties={
        'company': relationship(Company)})
    mapper(CourtesyPass, courtesy_pass_table, properties={
        'company': relationship(Company)
    })
    mapper(UserGroup, user_group_table)
    mapper(UserAccount, user_account_table, properties={
        'groups': relationship(UserGroup, 
                                secondary= lambda: user_group_association_table)
    })
    configure_mappers()


def create_db(metadata):
    metadata.create_all()


def drop_db(metadata):
    metadata.drop_all()
