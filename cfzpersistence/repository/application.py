from sqlalchemy.orm import aliased

from cfzpersistence.repository import InvalidIdTypeException, Repository
from cfzcore.application import Application, Status, ApplicationInvestor
from cfzcore.company import Company


class ApplicationRepository(Repository):
    def persist(self, application):
        '''
        Persist application instance to the database.
        '''
        self.DBSession.add(application)
        self.DBSession.flush()
        return application

    def all_with_status_new(self):
        '''
        Retrieve all applications with status NEW.
        '''
        company_alias = aliased(Company)
        application_records = self.DBSession.query(Application).join(company_alias, Application.company).filter(Application.status == Status.NEW).order_by(company_alias.name)
        return application_records.all()

    def get(self, id):
        '''
        Retrieve an instance of application that has a specific id.
        '''
        if not type(id) is int:
            raise InvalidIdTypeException("Id provided for application record is not a number")
        application = self.DBSession.query(Application).get(int(id))
        return application

    def get_investor(self, investor_id):
        '''
        Retrieve an investor from the database with an id of investor_id.
        '''
        investor = self.DBSession.query(ApplicationInvestor).get(int(investor_id))
        return investor

    def all_with_status_pending(self):
        '''
        Retrieve all applications with PENDING status.
        '''
        return self.DBSession.query(Application).filter(Application.status == Status.PENDING).all()

    def delete_investor(self, investor):
        '''
        Delete a specific investor from the database.
        '''
        self.DBSession.delete(investor)
        self.DBSession.flush()

    def persist_investor(self, investor):
        '''
        Persist an investor.
        '''
        self.DBSession.add(investor)
        self.DBSession.flush()

    def all_denied_applications(self):
        '''Retrieve all denied applications.'''
        return self.DBSession.query(Application).filter(Application.status == Status.DENIED).all()
