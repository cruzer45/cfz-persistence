from cfzpersistence.repository import Repository
from cfzcore.persistence.repository import (RepresentationTypeAbstractRepository,
                                            AuthorizedRepresentativeAbstractRepository)
from cfzcore.company import (RepresentationType,
                             AuthorizedRepresentative)

from sqlalchemy.orm import joinedload
from sqlalchemy.sql.expression import and_

class RepresentationTypeRepository(Repository, RepresentationTypeAbstractRepository):

    def all(self):
        return self.DBSession.query(RepresentationType).all()

    def get(self, id):
        return self.DBSession.query(RepresentationType).\
                filter(RepresentationType.id == int(id)).first()


class AuthorizedRepresentativeRepository(Repository, AuthorizedRepresentativeAbstractRepository):

    def all_for_company(self, company):
        return self.DBSession.query(AuthorizedRepresentative).\
            filter(and_(AuthorizedRepresentative.active == True,
                        AuthorizedRepresentative.company == company)).all()

    def get(self, id):
        return self.DBSession.query(AuthorizedRepresentative).\
                filter(and_(AuthorizedRepresentative.id == int(id),\
                            AuthorizedRepresentative.active == True)).first()

    def delete(self, authorized_representative):
        from datetime import datetime
        authorized_representative.active = False
        authorized_representative.date_deleted = datetime.now() 
        self.persist(authorized_representative)
        return authorized_representative
