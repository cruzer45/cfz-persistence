from cfzpersistence.repository import Repository
from cfzcore.persistence.repository import GoodsCategoryAbstractRepository
from cfzcore.goods import GoodsCategory


class GoodsCategoryRepository(Repository, GoodsCategoryAbstractRepository):

    def all(self):
        return self.DBSession.query(GoodsCategory)\
            .filter(GoodsCategory.active == True)\
            .all()

    def get(self, id):
        return self.DBSession.query(GoodsCategory).\
            filter(GoodsCategory.id == int(id)).\
            filter(GoodsCategory.active == True).first()

    def remove(self, good_category):
        good_category.active = False
        self.persist(good_category)
