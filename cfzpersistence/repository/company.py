from cfzpersistence.repository import Repository
from cfzpersistence.repository import NullRecordException
from cfzcore.company import Company, CompanyAnnualReport
from cfzcore.shareholder import Shareholder

from sqlalchemy import func
from sqlalchemy.sql.expression import and_
from sqlalchemy.orm import joinedload


class CompanyRepository(Repository):

    def all_authorized_companies(self):
        return self.DBSession.query(Company).\
            options(joinedload('directors')).\
            filter(Company.active == True).order_by(Company.name).all()

    def total(self):
        return self.DBSession.query(func.count(Company.id)).filter(Company.active == True).scalar()

    def get_by_pagination(self, offset=0, max_records=10):
        """Paginate all companies

        Keyword arguments:
        offset -- the page we want to retrieve
        max_records -- maximum number of records to retrieve
        """
        return self.DBSession.query(Company).\
            options(joinedload('directors')).\
            filter(Company.active == True).order_by(Company.name).\
            limit(max_records).\
            offset(offset).all()

    def search_for(self, company_name, offset=0, max_records=10):
        """Search for a company by name.

        We can paginate through the results if we provide 
        the appropriate key words.

        Keyword arguments:
        company_name -- the name of the company we are searching for.
        offset -- the page we want to retrieve
        max_records -- maximum number of records to retrieve
        """
        return self.DBSession.query(Company).\
            filter(and_(Company.active == True,
                        Company.name.ilike(company_name + "%"))).limit(max_records).\
            options(joinedload('directors')).offset(offset).all()

    def get(self, id):
        """Retrieve an instance of company given its id."""
        company = self.DBSession.query(Company).options(joinedload('directors')).\
                    filter(Company.id == int(id)).\
                    filter(Company.active == True).first()
        if company:
            return company
        else:
            raise NullRecordException("Company with id: %s does not exist!" % id)

    def get_shareholder_by_id(self, id):
        """Retrieve shareholder given an id"""
        shareholder = self.DBSession.query(Shareholder).options(joinedload('person')).\
                filter(and_(Shareholder.id == int(id),
                    Shareholder.active == True)).first()
        if shareholder:
            return shareholder
        else:
            raise NullRecordException("Shareholder with id: %s does not exist!" % id)

    def get_shareholders_for_company(self, company_id):
        """Retrieve all shareholders for a company"""
        shareholders = self.DBSession.query(Shareholder).\
                options(joinedload('person'), joinedload('company')).\
                filter(and_(Shareholder.company_id==int(company_id),
                    Shareholder.active == True)).all()
        return shareholders

    def annual_reports_for_company(self, company_id):
        """Retrieve all annual reports for the company"""
        annual_reports = self.DBSession.query(CompanyAnnualReport).\
                filter(CompanyAnnualReport.company_id==int(company_id)).all()
        return annual_reports

