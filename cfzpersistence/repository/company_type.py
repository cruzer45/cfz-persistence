from cfzpersistence.repository import Repository, NullRecordException
from cfzcore.company import CompanyType
from sqlalchemy.sql.expression import and_


class  CompanyTypeRepository(Repository):
    def get(self, id):
        company_type = self.DBSession.query(CompanyType).\
                            filter(CompanyType.id == int(id)).\
                            filter(CompanyType.active == True).first()
        if company_type:
            return company_type
        else:
            raise NullRecordException(u"CompanyType with id: %s does not exist!" % id)

    def find_by_name(self, name):
        return self.DBSession.query(CompanyType).\
                filter(and_(CompanyType._name == name, \
                    CompanyType.active == True)).first()

    def all(self):
        return self.DBSession.query(CompanyType)\
                .filter(CompanyType.active == True).all()
