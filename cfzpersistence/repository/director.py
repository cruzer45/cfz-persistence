from cfzpersistence.repository import (Repository, NullRecordException)
from cfzcore.company import Director


class DirectorRepository(Repository):
    
    def get(self, id):
        '''Retrieve the director with a given id.'''
        director = self.DBSession.query(Director).\
                  filter(Director.id == int(id)).first()
        if director:
            return director
        else:
            return NullRecordException("Director with id: %s does not exist!" % id)
