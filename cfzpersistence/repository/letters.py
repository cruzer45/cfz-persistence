from cfzpersistence.repository import Repository
from cfzcore.letters import AccessLetter
from cfzcore.persistence.repository import AccessLetterAbstractRepository
from datetime import datetime


class AccessLetterRepository(Repository, AccessLetterAbstractRepository):
    def get(self, id):
        return self.DBSession.query(AccessLetter).\
            filter(AccessLetter.id == int(id)).\
            filter(AccessLetter.active == True).\
            first()

    def list_by_letter_type(self, letter_type, offset=0, max_records=100):
        return self.DBSession.query(AccessLetter).\
            filter(AccessLetter.letter_type == letter_type).\
            filter(AccessLetter.active == True).\
            filter(AccessLetter.expiration_date > datetime.now()).\
            offset(offset).limit(max_records).\
            all()

    def list_expired_letters(self, letter_type, offset=0, max_records=100):
        return self.DBSession.query(AccessLetter).\
            filter(AccessLetter.letter_type == letter_type).\
            filter(AccessLetter.active == True).\
            filter(AccessLetter.expiration_date <= datetime.now()).\
            offset(offset).limit(max_records).\
            all()

    def count_by_letter_type(self, letter_type, expired=False):
        from sqlalchemy import func
        return self.DBSession.query(func.count(AccessLetter.id)).\
            filter(AccessLetter.active == True).\
            filter(AccessLetter.letter_type == letter_type).\
            filter(expired == (AccessLetter.expiration_date <= datetime.now())).\
            scalar()
