from cfzpersistence.repository import Repository, NullRecordException
from cfzcore.company.docs.contracts import OperationalContract


class OperationalContractRepository(Repository):
    
    def get(self, id):
        contract = self.DBSession.query(OperationalContract).\
                filter(OperationalContract.id == int(id)).\
                filter(OperationalContract.active == True).first()
        if contract:
            return contract
        else:
            raise NullRecordException(u"Operational Contract with id (%s) does not exist!" %id)
