from cfzpersistence.repository import Repository
from cfzcore.persistence.repository import CourtesyPassAbstractRepository
from cfzcore.company.passes import CourtesyPass

from sqlalchemy.sql.expression import and_


class CourtesyPassRepository(Repository, CourtesyPassAbstractRepository):
    def all_for_company(self, company):
        return self.DBSession.query(CourtesyPass).\
                filter(and_(CourtesyPass.company == company,
                            CourtesyPass.active == True)).all()

    def get(self, id):
        return self.DBSession.query(CourtesyPass).\
                filter(CourtesyPass.id == int(id)).first()

