from cfzpersistence.repository import (Repository, NullRecordException)
from cfzcore.shareholder import Shareholder


class ShareholderRepository(Repository):

    def get(self, id):
        '''Retrieve the shareholder with the given id.'''
        shareholder = self.DBSession.query(Shareholder).\
                      filter(Shareholder.id == int(id)).first()
        if shareholder:
            return shareholder
        else:
            return NullRecordException("Shareholder with id: %s does not exist!" %id)
