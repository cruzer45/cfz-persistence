from cfzpersistence.repository import Repository
from cfzpersistence.repository.company import CompanyRepository
from cfzcore.persistence.repository import VehiclePassAbstractRepository
from cfzcore.vehicle_pass import VehiclePass
from cfzcore.company import Company

from sqlalchemy.orm import joinedload
from sqlalchemy.sql.expression import and_


class VehiclePassRepository(Repository, VehiclePassAbstractRepository):

    def all(self):
        return self.DBSession.query(VehiclePass).\
                options(joinedload('company')).\
                filter(VehiclePass.active == True).all()

    def all_for_company(self, company):
        return self.DBSession.query(VehiclePass).\
            options(joinedload('company')).\
            filter(and_(VehiclePass.active == True,
                        VehiclePass.company == company)).all()

    def find_company_by_id(self, id):
        repository = CompanyRepository(self.DBSession)
        return repository.get(id)

    def search_by_owner(self, owner):
       return self.DBSession.query(VehiclePass).\
               filter(VehiclePass.owner.ilike(owner +"%")).all()

    def all_by_company_id(self, id):
        return self.DBSession.query(VehiclePass).\
                filter(VehiclePass.company_id == int(id)).all()

    def get(self, id):
        return self.DBSession.query(VehiclePass).\
                filter(VehiclePass.id == int(id)).\
                filter(VehiclePass.active == True).first()

    def get_active_vehiclepass(self, company_id):
        '''Retrieve all the active vehicle passes for a company.'''
        return self.DBSession.query(VehiclePass).\
                filter(and_(VehiclePass.company_id == int(company_id),
                            VehiclePass.expired == False)).all()

