import unittest

from cfzpersistence.mappings import do_map, create_db, drop_db
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from sqlalchemy.orm import (clear_mappers,
                            configure_mappers,
                            scoped_session,
                            sessionmaker)
from sqlalchemy.schema import MetaData
from sqlalchemy import create_engine

from cfzcore.company import Company
from cfzcore.person import Person
from cfzcore.interactors.vehicle_pass import create_vehicle_pass

from collections import namedtuple
import contextlib


__all__ = ["cfzpersistence"]
url = 'postgres://cfzadmin:cfz4dm1n@localhost:5432/cfz-test'
engine = create_engine(url)
metadata = MetaData(url)
maker = sessionmaker(autocommit=True)
DBSession = scoped_session(maker)


def clean_database():
    DBSession.close()
    clear_mappers()
    drop_db(metadata)


def create_database():
    from ..populate_default_values import create_user_groups
    clear_mappers()
    do_map(metadata)
    configure_mappers()
    create_user_groups(DBSession)


class DaoTest(unittest.TestCase):
    def setUp(self):
        self.DBSession = DBSession
        create_database()

    def tearDown(self):
        with contextlib.closing(engine.connect()) as con:
            trans = con.begin()
            for table in reversed(metadata.sorted_tables):
                con.execute(table.delete())
            trans.commit()
        DBSession.close()

    def create_company(self, name):
        from random import randint
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                   registration_number\
                                                   director')
        person = Person(u"Firstname", u"Lastname")
        director = Person(u"Director", u"First")
        params = CompanyTuple(name=name, manager=person, director=director,
                              registration_number=str(randint(1,99999)).encode('utf-8'))
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company

    def create_vehicle_pass(self, company, owner=u"Owner Name"):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        params = dict(company_id=company.id,
                      owner=owner,
                      vehicle_year=u'2000',
                      vehicle_model=u'Ford',
                      color=u"Blue",
                      license_plate=u'123341',
                      date_issued=u'22/12/2011',
                      sticker_number=u'212',
                      receipt_number=u'22',
                      month_expired=u'Dec')
        vehicle_pass = create_vehicle_pass(vehicle_pass_repository,
                                           **params)
        return vehicle_pass

    def create_user(self, user_name, password, email):
        from cfzcore.user_account import UserAccount
        user_account = UserAccount(user_name, email)
        user_account.set_password(password)
        return user_account
