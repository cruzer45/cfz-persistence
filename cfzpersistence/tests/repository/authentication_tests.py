from cfzpersistence.tests import(DaoTest,
                                 create_database,
                                 DBSession)
from cfzpersistence.repository.auth import (UserAccountRepository,
                                            DuplicateUserEmailException)
from cfzcore.user_account import UserAccount
from nose.tools import (eq_, raises)


class UserAccountsRepositoryTests(DaoTest):

    def test_should_persist_a_user_account(self):
        user_account_repository = UserAccountRepository(DBSession)
        user_account = self.create_user(u'New User', u'password', u'user@mail.com')
        user_account_repository.persist(user_account)
        self.assertIsNotNone(user_account.id)

    def test_should_save_encrypted_password(self):
        repository = UserAccountRepository(DBSession)
        user_account = self.create_user(u'New User', u'password', u'user@mail.com')
        repository.persist(user_account)
        self.assertNotEqual('password', user_account.passphrase, "Password should be encrypted")

    def test_should_find_user_by_email(self):
        repository = UserAccountRepository(DBSession)
        user_account = self.create_user(u'New User', u'password', u'user@mail.com')
        repository.persist(user_account)
        user_account = repository.find(u'user@mail.com')
        eq_(user_account.email, 'user@mail.com')

    def test_should_retrieve_all_user_accounts(self):
        repository = UserAccountRepository(DBSession)
        repository.persist(self.create_user(u'New User', u'password', u'user1@mail.com'))
        repository.persist(self.create_user(u'Second User', u'password', u'user2@mail.com'))
        repository.persist(self.create_user(u'Third user', u'password', u'user3@mail.com'))
        users = repository.all()
        eq_(3, len(users))

    def test_should_retreive_user_account_by_id(self):
        repository = UserAccountRepository(DBSession)
        user_account = repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))
        user = repository.get(user_account.id)
        eq_(user.id, user_account.id)

    def test_should_be_false_if_email_is_taken(self):
        repository = UserAccountRepository(DBSession)
        user_account = repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))
        eq_(False, repository.is_email_available(u'user@mail.com'))

    def test_should_be_true_if_email_is_available(self):
        repository = UserAccountRepository(DBSession)
        eq_(True, repository.is_email_available(u'user@mail.com'))

    @raises(DuplicateUserEmailException)
    def test_should_raise_exception_for_duplicate_emails(self):
        repository = UserAccountRepository(DBSession)
        user_account = repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))
        repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))

    def test_user_should_not_be_persisted_if_email_is_not_unique(self):
        repository = UserAccountRepository(DBSession)
        user_account = repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))
        try:
            repository.persist(self.create_user(u'New user', u'password', u'user@mail.com'))
        except DuplicateUserEmailException: 
            print "Did not persist"
        users = repository.all()
        eq_(1, len(users))


