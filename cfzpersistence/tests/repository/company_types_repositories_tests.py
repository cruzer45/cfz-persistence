from nose.tools import eq_, raises
from cfzpersistence.tests import (DaoTest, 
                                  create_database,
                                  DBSession)
from cfzcore.company import CompanyType
from cfzpersistence.repository.company_type import CompanyTypeRepository
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository import NullRecordException

class CompanyTypesRepositoryTests(DaoTest):

    def setUp(self):
        super(CompanyTypesRepositoryTests, self).setUp()
        self.repo = CompanyTypeRepository(DBSession)
        self.company_repo = CompanyRepository(DBSession)
        self.company = self.create_company(u"Company")

    def test_should_persist_a_company_type(self):
        import_type = self._create_company_type(u'Import/Export')
        self.assertIsNotNone(import_type.id)

    def test_should_retrieve_by_id(self):
        import_type = self._create_company_type(u'Import/Export')
        fetched_import_type = self.repo.get(import_type.id)
        eq_(import_type.id, fetched_import_type.id)
        eq_(import_type.name, fetched_import_type.name)

    def test_should_assign_company_type_to_company(self):
        import_type = self._create_company_type(u"Import/Export")
        self.company.company_types.append(import_type)
        self.company = self.company_repo.persist(self.company)
        _company = self.company_repo.get(self.company.id)
        eq_(1, len(_company.company_types))

    def test_should_remove_a_companys_company_type(self):
        import_type = self._create_company_type(u"Import/Export")
        manufacture_type = self._create_company_type(u"Manufacture")
        self.company.company_types.append(import_type)
        self.company.company_types.append(manufacture_type)
        self.company = self.company_repo.persist(self.company)
        self.company.company_types.remove(import_type)
        eq_(True, manufacture_type in self.company.company_types)
        eq_(False, import_type in self.company.company_types)

    def test_should_find_by_name(self):
        self._create_company_type(u'Import/Export')
        company_type = self.repo.find_by_name(u'Import/Export')
        self.assertIsNotNone(company_type.id)
        eq_(u'Import/Export', company_type.name)

    def test_should_retrieve_all_records(self):
        self._create_company_type(u'Import')
        self._create_company_type(u'Manufacture')
        self._create_company_type(u'Radio')
        self._create_company_type(u'ISP')
        eq_(4, len(self.repo.all()))


    @raises(NullRecordException)
    def test_should_raise_exception_if_record_does_not_exist(self):
        self.repo.get(1000000)

    def _create_company_type(self, name):
        return self.repo.persist(CompanyType.create(name))

