from cfzpersistence.repository.investor import InvestorRepository
from cfzpersistence.tests import DaoTest


class InvestorRepositoryTests(DaoTest):
    def test_should_retrieve_an_instance_of_investor(self):
        repository = InvestorRepository(self.DBSession)
        repository.get(1)
