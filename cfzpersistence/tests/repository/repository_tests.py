from cfzpersistence.repository import Repository
from cfzpersistence.tests import DaoTest
from cfzpersistence.config import get_session

from cfzcore.person import Person


class RepositoryTests(DaoTest):
    def test_should_persist_an_entity(self):
        person = Person("First", "Last")
        repository = Repository(get_session())
        repository.persist(person)
        self.assertIsNotNone(person.id)
