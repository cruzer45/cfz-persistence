from nose.tools import eq_

from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.shareholder import ShareholderRepository
from cfzpersistence.repository.company import CompanyRepository

from cfzcore.person import Person
from cfzcore.shareholder import Shareholder
from cfzcore.company import Company

from datetime import datetime
from collections import namedtuple

class ShareholderRepositoryTests(DaoTest):

    def test_should_persist_a_shareholder(self):
        repository = ShareholderRepository(DBSession)
        _company = self._create_company('Company')
        _shareholder = self._create_shareholder(**dict(first_name="First",
                                                       last_name="Last",
                                                       shares="250",
                                                       company=_company))
        shareholder = repository.persist(_shareholder)
        self.assertIsNotNone(shareholder.id)


    def test_should_retrieve_shareholder_by_id(self):
        repository = ShareholderRepository(DBSession)
        _company = self._create_company('Company')
        shareholder = self._create_shareholder(**dict(first_name="First",
                                                       last_name="Last",
                                                       shares="250",
                                                       company=_company))
        shareholder = repository.persist(shareholder)
        retrieved_shareholder = repository.get(shareholder.id)
        eq_(retrieved_shareholder.id, shareholder.id)



    def _create_shareholder(self, **kwargs):
        _shareholder = Shareholder(**kwargs)
        return _shareholder



    def _create_company(self, name):
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                     registration_number\
                                                     company_type')
        person = Person("Firstname", "Lastname")
        params = CompanyTuple(name=name, manager=person,
                              registration_number='12311',
                              company_type='Import/Export')
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company

