from cfzpersistence.tests import(DaoTest,
                                 create_database,
                                 DBSession)
from nose.tools import eq_
from cfzpersistence.repository.auth import UserGroupRepository
from cfzcore.user_account import UserGroup


class UserGroupRepositoryTests(DaoTest):
    def test_should_persist_a_user_group(self):
        repository = UserGroupRepository(DBSession)
        user_group = UserGroup('clerks')
        user_group = repository.persist(user_group)
        self.assertIsNotNone(user_group.id)

    def test_should_find_user_group_by_name(self):
        repository = UserGroupRepository(DBSession)
        user_group = UserGroup('clerks')
        repository.persist(user_group)
        group = repository.find('clerks')
        eq_(group.id, user_group.id)

    def test_should_retrieve_user_group_by_id(self):
        repository = UserGroupRepository(DBSession)
        user_group = UserGroup('clerks')
        repository.persist(user_group)
        eq_(user_group.id, repository.get(user_group.id).id)
