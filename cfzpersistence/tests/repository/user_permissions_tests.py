from cfzpersistence.tests import (DaoTest,
                                  DBSession)

from cfzcore.user_account import (UserAccount,
                                  UserGroup)
from cfzpersistence.repository.auth import (UserAccountRepository,
                                            UserGroupRepository)
from nose.tools import eq_


class UserPermissionsTests(DaoTest):

    def test_should_remove_permissions_from_user(self):
        user_repo = UserAccountRepository(DBSession)
        user_group_repo = UserGroupRepository(DBSession)
        user = self.create_user(u'New User', u'password', u'user@mail.com')
        company_create_perm = self._company_create_permission()
        company_view_perm = self._company_view_permission()
        outlet_perm = self._outlet_create_permission()
        user.add_to_group(company_create_perm)
        user.add_to_group(company_view_perm)
        user.add_to_group(outlet_perm)
        user = user_repo.persist(user)
        user_repo.remove_permissions(user, (company_create_perm, outlet_perm))
        eq_(True, len(user.groups))
        eq_(False, company_create_perm in user.groups)
        eq_(False, outlet_perm in user.groups)
        eq_(True, company_view_perm in user.groups)

    def test_should_add_a_list_of_permissions(self):
        user_repo = UserAccountRepository(DBSession)
        user_group_repo = UserGroupRepository(DBSession)
        user = self.create_user(u'New User', u'password', u'user@mail.com')
        company_create_perm = self._company_create_permission()
        company_view_perm = self._company_view_permission()
        outlet_perm = self._outlet_create_permission()
        user = user_repo.add_permissions(user, (company_create_perm, company_view_perm, outlet_perm))
        eq_(3, len(user.groups))
        eq_(True, company_create_perm in user.groups)
        eq_(True, outlet_perm in user.groups)
        eq_(True, company_view_perm in user.groups)

    def _company_create_permission(self):
        user_group_repo = UserGroupRepository(DBSession)
        return user_group_repo.find(u'company:create')

    def _company_view_permission(self):
        user_group_repo = UserGroupRepository(DBSession)
        return user_group_repo.find(u'company:view')

    def _outlet_create_permission(self):
        user_group_repo = UserGroupRepository(DBSession)
        return user_group_repo.find(u'company:outlet:create')

