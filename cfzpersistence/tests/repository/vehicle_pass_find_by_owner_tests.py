from nose.tools import eq_

from cfzpersistence.tests import DaoTest
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository


class VehiclePassRepositoryFindByOwnerTests(DaoTest):

    def test_should_find_a_vehicle_pass_whose_owner_name_matches_a_partial_name(self):
        vehicle_pass_repository = VehiclePassRepository(self.DBSession)
        company = self.create_company(u"Company Name")
        owner_name = u"Owner OfVehicle"
        for cnt in range(0, 10):
            self.create_vehicle_pass(company, owner_name+str(cnt)) 
        vehicle_passes_for_owner_name = vehicle_pass_repository.search_by_owner(owner_name)
        eq_(10, len(vehicle_passes_for_owner_name))

    def test_should_return_all_vehicle_passes_whose_owner_name_matches_a_complete_name(self):
        vehicle_pass_repository = VehiclePassRepository(self.DBSession)
        company = self.create_company(u"Company Name")
        owner_name = u"Owner of Vehicle"
        for cnt in range(0, 10):
            self.create_vehicle_pass(company, owner_name+str(cnt))
        vehicle_passes_for_owner = vehicle_pass_repository.search_by_owner(owner_name+str(1))
        eq_(1, len(vehicle_passes_for_owner))
