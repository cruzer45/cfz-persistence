from nose.tools import eq_

from cfzpersistence.tests import DaoTest 
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository



class VehiclePassRepositoryFindCompanyByIdTests(DaoTest):

    def test_should_retrieve_company_for_a_vehicle_pass(self):
        self.vehicle_pass_repository = VehiclePassRepository(self.DBSession)
        company1 = self.create_company('Company')
        company = self.vehicle_pass_repository.find_company_by_id(company1.id)
        eq_(company1.id, company.id)
