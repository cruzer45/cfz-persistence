from nose.tools import eq_

from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.person import Person
from cfzcore.company import Company
from cfzcore.vehicle_pass import VehiclePass

from collections import namedtuple
from datetime import datetime


class VehiclePassRepositoryListPassesTests(DaoTest):

    def test_should_list_all_vehicle_passes(self):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        company1 = self._create_company('Company1')
        for cnt in range(0, 100):
            vehicle_pass = self._create_vehicle_pass(company1)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        company2 = self._create_company('Company2')
        for cnt in range(0, 10):
            vehicle_pass = self._create_vehicle_pass(company2)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        passes = self.vehicle_pass_repository.all()
        eq_(110, len(passes))

    def test_should_list_all_vehicle_passes_for_a_company(self):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        company1 = self._create_company('Company1')
        for cnt in range(0, 100):
            vehicle_pass = self._create_vehicle_pass(company1)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        company2 = self._create_company('Company2')
        for cnt in range(0, 10):
            vehicle_pass = self._create_vehicle_pass(company2)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        passes = self.vehicle_pass_repository.all_for_company(company1)
        eq_(100, len(passes))

    def test_should_find_passes_by_company_id(self):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        company1 = self._create_company('Company1')
        for cnt in range(0, 100):
            vehicle_pass = self._create_vehicle_pass(company1)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        company2 = self._create_company('Company2')
        for cnt in range(0, 10):
            vehicle_pass = self._create_vehicle_pass(company2)
            vehicle_pass.active = True
            self.vehicle_pass_repository.persist(vehicle_pass)
        passes = self.vehicle_pass_repository.all_by_company_id(company1.id)
        eq_(100, len(passes))


    def _create_company(self, name):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                     registration_number\
                                                     company_type')
        person = Person("Firstname", "Lastname")
        params = CompanyTuple(name=name, manager=person,
                              registration_number='12311',
                              company_type='Import/Export')
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company

    def _create_vehicle_pass(self, company):
        self.vehicle_pass_repository = VehiclePassRepository(DBSession)
        params = dict(owner=u'Owner Name',
                      company=company,
                      vehicle_year=u'2000',
                      vehicle_model=u'Mazda',
                      color=u'white',
                      license_plate=u'12311',
                      date_issued=datetime.strptime('25/5/2012', '%d/%m/%Y'),
                      sticker_number=u'12312',
                      receipt_number=u'2413',
                      month_expired=u'Jan')
        vehicle_pass = VehiclePass(**params)
        return vehicle_pass
