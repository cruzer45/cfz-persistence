from nose.tools import eq_ 
from cfzpersistence.tests import (DaoTest,
                                  create_database,
                                  DBSession)
from cfzpersistence.repository.company import CompanyRepository
from cfzpersistence.repository.vehicle_pass import VehiclePassRepository

from cfzcore.person import Person
from cfzcore.company import Company
from cfzcore.vehicle_pass import VehiclePass

from collections import namedtuple
from datetime import datetime


class VehiclePassRepositoryTests(DaoTest):

    def test_should_persist_a_vehicle_pass(self):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        company = self._create_company('Company_')
        vehicle_pass = self._create_vehicle_pass(company)
        vehicle_pass = vehicle_pass_repository.persist(vehicle_pass)
        self.assertIsNotNone(vehicle_pass.id)

    def test_should_get_all_active_vehiclepass_for_a_company(self):
        vehiclepass_repository = VehiclePassRepository(DBSession)
        company = self._create_company('Company')
        self._create_active_vehiclepass(company, 10)
        total_active_vehicelpass = vehiclepass_repository.get_active_vehiclepass(company.id)
        eq_(10, len(total_active_vehicelpass))

    def test_should_ignore_expired_vehiclepass_when_retrieving_active_passes(self):
        vehiclepass_repository = VehiclePassRepository(DBSession)
        company = self._create_company('Company')
        self._create_active_vehiclepass(company, 10)
        self._create_expired_vehiclepass(company, 5)
        total_active_vehicelpass = vehiclepass_repository.get_active_vehiclepass(company.id)

    def _create_active_vehiclepass(self, company, total):
        vehiclepass_repository = VehiclePassRepository(DBSession)
        for i in range(total):
            vehiclepass_repository.persist(self._create_vehicle_pass(company))

    def _create_expired_vehiclepass(self, company, total):
        vehiclepass_repository = VehiclePassRepository(DBSession)
        for i in range(total):
            _vehiclepass = self._create_vehicle_pass(company)
            _vehiclepass.expired = True
            vehiclepass_repository.persist(_vehiclepass)

    def _create_company(self, name):
        CompanyTuple = namedtuple('CompanyTuple', 'name manager\
                                                     registration_number\
                                                     company_type')
        person = Person("Firstname", "Lastname")
        params = CompanyTuple(name=name, manager=person,
                              registration_number='12311',
                              company_type='Import/Export')
        company = Company(params)
        company.active = True
        repository = CompanyRepository(DBSession)
        repository.persist(company)
        return company

    def _create_vehicle_pass(self, company):
        vehicle_pass_repository = VehiclePassRepository(DBSession)
        params = dict(owner=u'Owner Name',
                      company=company,
                      vehicle_year=u'2000',
                      vehicle_model=u'Mazda',
                      color=u'white',
                      license_plate=u'12311',
                      date_issued=datetime.strptime('25/5/2012', '%d/%m/%Y'),
                      sticker_number=u'12312',
                      receipt_number=u'2413',
                      month_expired=u'Jan')
        vehicle_pass = VehiclePass(**params)
        return vehicle_pass
