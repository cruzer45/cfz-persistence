import os
import sys
from ConfigParser import SafeConfigParser
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.schema import MetaData
from cfzpersistence.mappings import do_map, create_db
from cfzpersistence.repository.auth import (
        UserAccountRepository,
        UserGroupRepository)
from cfzcore.user_account import UserAccount, UserGroup
from cfzcore.company import RepresentationType


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
            '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def init_session(db_url):
    engine = create_engine(db_url)
    metadata = MetaData(db_url)
    return metadata

def create_goods_categories(DBSession):
    '''Create a default list of categories.'''
    from cfzpersistence.repository.categories import GoodsCategoryRepository
    from cfzcore.goods import GoodsCategory
    goods_repo = GoodsCategoryRepository(DBSession)
    if len(goods_repo.all()) < 1:
        clothing = GoodsCategory(u'Clothing')
        footwear = GoodsCategory(u'Footwear')
        handbags = GoodsCategory(u'Handbags')
        liquor = GoodsCategory(u'Liquor')
        cigarettes = GoodsCategory(u'Cigarettes')
        goods_repo.persist(clothing)
        goods_repo.persist(footwear)
        goods_repo.persist(handbags)
        goods_repo.persist(liquor)
        goods_repo.persist(cigarettes)


def create_representation_type(DBSession):
    from cfzpersistence.repository.authorized_representative import RepresentationTypeRepository
    broker = RepresentationType(u'Broker')
    director = RepresentationType(u'Director')
    legal = RepresentationType(u'Legal')
    repository = RepresentationTypeRepository(DBSession)
    if len(repository.all()) < 1:
        repository.persist(broker)
        repository.persist(director)
        repository.persist(legal)


def create_admin(DBSession):
    group_repo = UserGroupRepository(DBSession)
    user_repo = UserAccountRepository(DBSession)
    admin_group = group_repo.find(u'admin')
    admin_user = user_repo.find(u'admin@cfz.com')
    if admin_group and not admin_user:
        admin_user = UserAccount(u'Admin', u'admin@cfz.com')
        admin_user.set_password(u'changeme')
        admin_user.add_to_group(admin_group)
        user_repo.persist(admin_user)


if __name__ == '__main__':
    from cfzpersistence.populate_default_values import (create_user_groups,
                                                        create_company_types)
    if len(sys.argv) !=2:
        usage(sys.argv)
    parser = SafeConfigParser()
    parser.read(sys.argv[1])
    db_url = parser.get('db', 'url')
    metadata = init_session(db_url)
    maker = sessionmaker(autocommit=True)
    DBSession = scoped_session(maker)
    do_map(metadata)
    create_db(metadata)
    create_user_groups(DBSession)
    create_admin(DBSession)
    create_goods_categories(DBSession)
    create_representation_type(DBSession)
    create_company_types(DBSession)
